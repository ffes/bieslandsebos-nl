---
layout: post
title: Werkzaamheden parkeerplaats
date: 2023-07-19 20:26:23 +02:00
lang: nl
frontpage:
  until: 2023-08-25 23:59:59 +02:00
  excerpt: Van 24 juli t/m 25 augustus zijn er werkzaamheden op de parkeerplaats
---

Vanaf maandag 24 juli voert Staatsbosbeheer werkzaamheden uit in de bosstrook tegenover ons terrein, bekend als "De Balij".
Hierbij worden diverse bomen omgezaagd en later versnippert.
Voor een deel van deze werkzaamheden wordt van "onze" parkeerplaats gebruik gemaakt.
Dit heeft daardoor flinke gevolgen voor ons terrein en de kampeerders.

De verwachting is dat vanaf dinsdag 22 augustus er een houtversnipperaar ingereden zal worden die het gezaagde hout gaat versnipperen.
Op het parkeerterrein worden deze snippers tijdelijk gestort totdat ze worden afgevoerd naar een biomassa&shy;centrale.
De parkeerplaats wordt gedurende deze werkzaamheden met bouwhekken afgesloten.

Dit betekent dat het parkeerterrein vanaf maandagavond 21 augustus leeg zijn!
De dichtstbijzijnde beschikbare parkeerterrein is dan die bij de Dobbeplas.
De verwachting is dat deze afsluiting de rest van de werkweek gaat duren en vanaf 25 augustus eind van de middag het parkeerterrein weer beschikbaar is.
