# Website Bieslandse Bos

## Over

Dit is de repository waarmee de website van [Scoutingkampeerterrein Bieslandse Bos](http://www.bieslandsebos.nl) wordt ontwikkeld en beheerd.
Wij gebruiken [Jekyll](https://jekyllrb.com/) om een statische website te genereren.

Mocht je een verbetering voor onze website willen voorstellen, dan accepteren wij goede [Merge Requests](https://gitlab.com/bieslandsebos/bieslandsebos.gitlab.io/-/merge_requests).


## Translation Errors

If you find any translation or grammar error in the English or German site, please [file an issue](https://gitlab.com/bieslandsebos/bieslandsebos.gitlab.io/-/issues/new) or create a [Merge Request](https://gitlab.com/bieslandsebos/bieslandsebos.gitlab.io/-/merge_requests).
Please tell in your issue which page (the URL from the address bar), what is says now and what it should be.


## Building op Linux

Op Linux kun je de site het simpelste draaien in een docker container.
Je hoeft dan alleen [docker-ce te installeren](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/).

```sh
echo "host: 0.0.0.0" > _config-dev.yml
npm run docker
```

Je wilt waarschijnlijk ook deze regels in `_config-dev.yml`, zodat tijdens het ontwikkelen de css sourcemap wordt aangemaakt.

```yml
sass:
  sourcemap: always
```


## Building op Windows

De site is lokaal te bouwen met [Windows 10 WSL2](https://docs.microsoft.com/en-us/windows/wsl/) en [Docker Desktop](https://www.docker.com/products/docker-desktop).

```sh
echo host: 0.0.0.0 > _config-dev.yml
docker run --rm -ti --volume %CD%:/jekyll/ -p 4000:4000 -p 35729:35729 jekyll/jekyll /bin/bash
```

## Building de site

[NodeJS](https://nodejs.org) wordt gebruikt om [Bootstrap Theming](https://getbootstrap.com/docs/4.0/getting-started/theming/) te vergemakkelijken en andere dependencies wordt gedownload, zoals [Font Awesome](https://fontawesome.com/).

Jekyll wordt via de `bundler` uitgevoerd. Om de build-stappen eenvoudiger te maken is een `Makefile` beschikbaar.

Als de docker image actief is kunnen we de site gaan bouwen.

In de container ga naar de directory `/jekyll` en voer de volgende build stappen uit.

Eerst alle voorbereidingen treffen.

```
make prepare
```

De npm postinstall hook maakt een `lib` directory aan waar bestanden vanuit de diverse `node_modules` naartoe worden gekopieerd zodat ze bij elkaar op een plek staan. Daardoor is de `node_modules` directory alleen nodig tijdens het build-proces.

En dan om lokaal te builden en te testen

```
make livereload
```

Open http://localhost:4000/ om te site te bekijken.


## Opbouw site

### Algemeen

- Het overgrote deel van de site is drietalig beschikbaar (NL, EN, DE). Alleen enkele vrij specifieke pagina's (zoals bijvoorbeeld de vactures) of onderdelen van activiteiten die alleen in het Nederlands beschikbaar zijn niet.
- Er is bewust voor gekozen om niet alle pagina die er zijn in een menu op te nemen. Dus bij sommige pagina's moet altijd via een andere pagina komen.
  Hierdoor wordt het menu klein gehouden, want met name bij mobiele weergave prettig is.
- De site is vooral voor praktische informatie voor potentiele kampeerders.
- Er is een nieuws-sectie, maar dat is vooral voor "officiele" mededelingen. Deze hebben een einddatum voor de voorpagina, het archief staat op de about-pagina. Leuke nieuwtjes zetten we op onze Facebook-pagina.
- We tutoyeren onze lezers, in iedere geval op de Nederlandse pagina's.

### Data-driven pagina's

Diverse pagina's worden gegenereerd op basis van JSON-bestanden die in `_data`-directory te vinden zijn. Dit gaat om de pagina's met prijzen, informatie over algemene faciliteiten en de velden, en met uitstapjes buiten het terrein.

Hier is mede voor gekozen om de vertaalbaarheid van de pagina's naar Engels en Duits te vergemakkelijken. Bijvoorbeeld een prijsaanpassing hoeft dan slechts 1x te worden gedaan en de pagina's in alle talen worden automatisch bijgewerkt.

### Pagina's voor de velden

Er wordt per veld een losse pagina gegeneerd die (momenteel) niet direct vindbaar is vanuit de site.
Deze pagina's worden gemaakt zodat in de bevestiging naar de kampeerders een deep-link naar de gehuurde pagina opgenomen kan worden.
Voor deze functionaliteit wordt gebruik gemaakt van de [Jekyll Data Pages Generator-plugin](https://github.com/avillafiorita/jekyll-datapage_gen).


## Deployment

De staging/test omgeving is te vinden op https://bieslandsebos.gitlab.io/
Deze wordt bij iedere `git push` naar `main` automatisch opnieuw gegenereerd door GitLab CI.

Om de site op de productie-locatie te zetten, push een annotated tag met een ISO-datum en GitLab CI zorgt voor de rest.
