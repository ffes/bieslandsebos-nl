---
# Front matter comment to ensure Jekyll properly generates the file
---

const news = {
	"items": [
{% for post in site.posts %}
	{% if site.time < post.frontpage.until %}
		{
			"van": "{{ post.date }}",
			"tot": "{{ post.frontpage.until }}",
			"kop": "{{ post.title }}",
			"tekst": "<a href=\"{{ post.url }}\">{{ post.frontpage.excerpt }}</a>"
		},
	{% endif %}
{% endfor %}
		{
			"van": "2023-06-14 00:00:01",
			"tot": "2023-07-05 23:59:59",
			"kop": "Stookverbod",
			"tekst": "Vanwege de aanhoudende droogte geldt er op dit moment helaas een stookverbod op het terrein. Meer informatie op <a href=\"https://www.natuurbrandrisico.nl\">natuurbrandrisico.nl</a>."
		}
	]
}

// Door de nieuwsitems gaan en indien nodig toevoegen
for (let i = 0; i < news.items.length; i++)
{
	// Moet dit nieuwsitem getoond worden
	if (Date.now() < Date.parse(news.items[i].van) || Date.now() > Date.parse(news.items[i].tot))
		continue;

	// Ieder nieuwsitem krijgt zijn eigen "div"
	const row = document.createElement("div");

	// Zet de kop erboven
	const h2 = document.createElement("h2");
	h2.innerHTML = news.items[i].kop;
	row.appendChild(h2);

	// De tekst van het bericht toevoegen
	const p = document.createElement("p");
	p.innerHTML = news.items[i].tekst;
	row.appendChild(p);

	// De "row" in het document plaatsen
	var content_heading = document.getElementsByClassName("content-heading");
	for (let j = 0; j < content_heading.length; j++)
	{
		content_heading[j].appendChild(row);
	}
}
