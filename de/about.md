---
layout: default
title: Über uns
lang: de
---

# Über Bieslandse Bos

Der Pfadfinder­zeltplatz "Het Bieslandse Bos" ist ein Zeltplatz für Pfadfinders die 1992 eröffnet wurde.

Der Zeltlatz wird von einer Stiftung unterhalten, in der Volontäre mit einem Herz für Scouting dafür sorgen, dass der Campingplatz ein sicherer und glücklicher Aufenthaltsort ist.

## Facebook

Folgen Sie uns auf [Facebook]({{ site.social.facebook }}) <i class="bi bi-facebook text-success"></i>, wo wir Informationen über aktuelle Aktivitäten und Fotos veröffentlichen.

## Website

Erkennen Sie einen Fehler auf unserer Website oder sehen Sie etwas anderes, das verbessert werden kann? Sie können helfen. Der Quellcode unserer Website ist bei [GitLab](https://gitlab.com/bieslandsebos/bieslandsebos.gitlab.io) verfügbar und wir akzeptieren gute Merge Requests.
