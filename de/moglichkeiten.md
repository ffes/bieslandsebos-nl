---
layout: default
title: Möglichkeiten
lang: de
---

# Möglichkeiten auf oder von der Zeltplatz

<div class="col-md-4 float-end">
	<img class="img-rounded img-fluid float-end" src="{% link assets/images/tent-op-palen.jpg %}" alt="Blik op het terrein" />
</div>
Der Zeltplatz liegt in einem wunderschönen Natur- und Erholungsgebiet.
Es gibt viele Möglichkeiten, dies zu genießen. Und mit verschiedenen Städten in der Nähe gibt es viele Möglichkeiten für [eine Vielzahl von Aktivitäten außerhalb des Zeltplatzes]({% link de/nahe.html %}).


## Große Feuerstelle: Phoenix

{% assign phoenix = site.data.terrein.faciliteiten | where: "id", "phoenix" | first %}
{% for txt in phoenix.de %}
{{ txt }}
{% endfor %}


## Hindernislauf

{% assign trappersbaan = site.data.terrein.faciliteiten | where: "id", "trappersbaan" | first %}
{% for txt in trappersbaan.de %}
{{ txt }}
{% endfor %}


## Erholungssee

Neben dem Platz gibt es einen großen Teich: den "Dobbeplas". Dieser ist Teil des bereits angelegten Erholungsgebietes, das sich von Delft über Pijnacker-Nootdorp bis in Zoetermeer erstreckt.

Im Teich kann man surfen, es gibt einen Sandstrand, eine Spiel- und Liegewiese. Er eignet sich somit sehr zum Floß fahren, rudern, schwimmen oder angeln. Die Pfade im Gebiet laden zum Wandern ein. Die Naturinsel ist ein kleines Paradies für Vogel- und Naturliebhaber.

[Material zum Flösse bauen]({% link assets/downloads/vlot-de.pdf %}) gibt es auf dem Platz und zwei 3-Personen-Kanus kann man beim Platzwart mieten.

Tipp: Wenn jeder mit den Flössen beschäftigt ist, kann ein Kanu für die Leitung nützlich sein: du bist schnell vor Ort falls etwas schief geht.


## Hallenbad

In Gehweite in Pijnacker befindet sich das Hallenbad [De Viergang](https://www.deviergang.nl/). Dieser Pool ist Teil eines [größeren Komplexes](https://www.viergang.nl/) mit Squash, Fitness, Sauna usw.
