---
layout: default
title: Reservieren
lang: de
permalink: /de/reservieren/tnx.html
---

# Reservieren oder Auskünfte beantragen

Sie bitten, wir antworten. Weil eben erst nach unserer Antwort die Reservierung festliegt.

Danke für deine Anfrage. Im Allgemeinen antworten wir in einigen Tagen. Wenn Sie nach einer Woche noch nichts von uns gehört hast, mail dann mal unser Reservierungsbüro: [{{ site.email }}](mailto:{{ site.email }})
