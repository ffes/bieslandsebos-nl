---
layout: default
title: Mogelijkheden
---

# Mogelijkheden op of vanaf het terrein

<div class="col-md-4 float-end">
	<img class="img-rounded img-fluid float-end" src="{% link assets/images/tent-op-palen.jpg %}" alt="Blik op het terrein" />
</div>
Het Bieslandse Bos ligt in een schitterend natuur- en recreatiegebied midden in de Randstad. Er zijn diversen mogelijkheden om hiervan optimaal te genieten.
En met diverse steden in de omgeving zijn er genoeg opties voor [een leuke activiteit buiten het terrein]({% link nl/omgeving.html %}).

## Activiteitenkisten

Programma-boxen om zélf de natuur te ontdekken.

We hebben de beschikking over een set van vier natuuractiviteiten ontwikkeld voor Welpen, Scouts en Explorers, om samen de natuur te ontdekken. De activiteiten zijn zo ontwikkeld dat je, ook als je weinig kennis en ervaring met natuuractiviteiten hebt, er direct mee aan de slag kunt. Aangezien bijna alle materialen in de set aanwezig zijn, hoef je maar weinig voor te bereiden.

### Wat loopt daar in mijn tent?
Een activiteit voor Scouts en Explorers over het zoeken en op naam brengen van kleine diertjes, zoals spinnen, vliegen, oorwurmen, pissebedden en wormen. Deze activiteit is met een kleine aanpassing ook geschikt voor Welpen.

### Zo smaakt een kampeerterrein!
Deze activiteit geeft alle informatie om met je Scouts of Explorers een maaltijd te bereiden met wilde planten die je op of vlakbij het terrein kunt vinden.

### Scoren met sporen!
Een activiteit waarbij Scouts en Explorers op zoek gaan naar sporen van dieren. Het spoorzoeken kan eventueel ook met welpen gedaan worden.

### Samoerai
'Samoerai, de Japanse natuurontdekker' is een natuurbelevingsactiviteit voor Welpen die kamperen op het labelterrein.

Vraag de kampwacht of zij jouw activiteit te leen hebben. Op een eenvoudige manier heb je dan tijdens een ochtend of middag van je kamp een leuk programma.

Gebruik van de sets is gratis, maar voor de materialen vragen we wel een borg van € 30,-


## Onze kampvuurcirkel: Phoenix

{% assign phoenix = site.data.terrein.faciliteiten | where: "id", "phoenix" | first %}
{% for txt in phoenix.nl %}
{{ txt }}
{% endfor %}

## Trappersbaan

{% assign trappersbaan = site.data.terrein.faciliteiten | where: "id", "trappersbaan" | first %}
{% for txt in trappersbaan.nl %}
{{ txt }}
{% endfor %}

## Loophike vanaf het terrein (6½ km)

Een wandeltocht met start- en eindpunt op het terrein is een prima manier in 2 tot 3 uur te genieten van de directe omgeving.
Er is een [routebeschrijving]({% link assets/downloads/route-kort-2021-07-20.zip %}), voorzien van diverse eenvoudige hiketechnieken. In de zip wordt ook een gpx-bestand en een aanvullende uitleg meegeleverd.

## Meerdaagse hikes

We hebben met [Kampeerterrein Staelduin](https://staelduin.scouting.nl/) een gesloten beurzen afspraak gemaakt.
Bij het ene terrein op kamp, dan is de hikeovernachting op het andere terrein bij de prijs inbegrepen.

Als je al weet dat je tijdens je zomerkamp van ons terrein naar Kampeerterrein Staelduin wilt hiken, reserveer dan je overnachting alvast op Staelduin.
Je kan in het opmerkingenveld aangeven dat je die week op het Bieslandse Bos kampeert, dan worden er geen extra kosten in rekening gebracht.

### Route naar Kampeerterrein Staelduin

Gezien het vele verkeer van met name hikers tussen ons terrein en onze collega's van [Kampeerterrein Staelduin](https://staelduin.scouting.nl/) is een fietsroute van ongeveer 25 km van Staelduin naar het Bieslandse Bos en andersom. Heen- én terugweg dus.

Het is een tocht die door Delft gaat, met mogelijkheden om de binnenstad te bezoeken, en op de terugweg langs de Dobbeplas komt.

Download het [bestand]({% link assets/downloads/hike-bb-staelduin-2023.pdf %}), druk de twee bladzijden dubbelzijdig af en vouw hem in drieën.

## Bosonderhoud

Het terrein is eind jaren 80 aangeplant met populieren. Jaarlijks planten we honderden bomen en struiken om meer variatie te krijgen, en vooral om een bos over te houden als de populieren straks aan hun einde komen. Regelmatig moeten we bomen weghalen omdat ze slecht zijn, en daarmee gevaarlijk tijdens harde stormen.

Met name in het winterseizoen doen wij bosonderhoud, [en daar kan jij bij helpen]({% link nl/vacatures.md %})!

## Recreatieplas

Naast het terrein ligt een grote speelvijver: de Dobbeplas. Deze maakt onderdeel uit van het recreatiegebied, wat loopt van de Delftse Hout via Nootdorp, Pijnacker tot de A12 bij Zoetermeer.

De plas heeft een surfoever, zandstrand, speel- en ligweide en diverse paden en is dus ideaal voor wandelen, vlotvaren, surfen, roeien, zwemmen of vissen. Bij de molen is een skeelerbaan en de zuidoostelijke kant is een paradijsje voor vogel- en natuurliefhebbers.

Controleer altijd de [waterkwaliteit](https://www.zwemwater.nl/home?id=1923) van de Dobbeplas.

Materiaal voor [vlottenbouw]({% link assets/downloads/vlot-nl.pdf %}) is op het terrein aanwezig, en 2 driepersoons kano's zijn te huur bij de kampwacht.

Tip: Als iedereen met vlotten bezig is, dan is een kano handig voor de leiding. Je bent snel ter plaatse mocht er iets geks gebeuren.

Iets verder weg ligt de recreatieplas van de Delftse Hout, vergelijkbaar met de Dobbeplas. Controleer de [waterkwaliteit](https://www.zwemwater.nl/home?id=1920) van de Delftse Hout.

## Zwembad

Op loopafstand van het terrein ligt in Pijnacker zwembad [De Viergang](https://www.deviergang.nl/). Dit bad is onderdeel van een [groter sportcomplex](https://www.viergang.nl/) met squash, fitness, sauna etc. In elk geval een mooi bad om lekker schoon te weken voordat iedereen weer naar huis gaat.

## Acker- en Biesland Fietsroute

De gemeente heeft een [bewegwijzerde fietstocht](https://www.pijnacker-nootdorp.nl/direct-regelen/sport-cultuur-en-recreatie/wandel-en-fietsroutes-door-recreatiegebieden/33322) uitgezet die ook langs ons terrein loopt, dus start en finish zijn vlakbij.
De route is een ronde van 20 km of 30 km door het gebied om ons heen die met bordjes is aangegeven!
