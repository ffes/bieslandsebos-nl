---
layout: default
title: Bedankt voor de informatieaanvraag
permalink: /nl/reserveren/tnx.html
---

# {{ page.title }}

Jij vraagt, wij antwoorden. Maar pas na ons antwoord is het definitief.

Dank je voor jouw aanvraag. Meestal geven we antwoord binnen enkele dagen. Mocht je na een week nog niets van ons gehoord hebben, mail dan even met het reserveringsbureau: [{{ site.email }}](mailto:{{ site.email }})
