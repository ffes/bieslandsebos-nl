---
layout: default
title: Geschiedenis
---

# Geschiedenis Bieslandse Bos

De noodzaak voor een nieuw kampeerterrein in de regio Den Haag was ruim aanwezig toen eind jaren 80 het terrein Raaphorst in Wassenaar niet langer tot onze beschikking stond. Lange tijd is er met man en macht gezocht naar vervanging en in 1989 kwam er van Staatsbosbeheer een stukje grond beschikbaar tussen Nootdorp en Pijnacker.

Dit stukje grond meet 80 x 300 meter, en stond vol met (allemaal dezelfde) populieren. Netjes in rijtjes, om de 4 meter. Voorop het terrein stond een stal van onze latere buurman, Dhr. Brouwer. In deze schuur stonden 8 koeien en lag 25 cm mest. Brouwer had die schuur graag voor zijn pinken willen gebruiken, maar Staatsbosbeheer vond dat niet goed. Sterker nog, Brouwer moest die schuur afbreken. Doordat wij het terrein in gebruik namen -en die schuur wel handig vonden- hebben wij deze verplichting overgenomen, mochten we ooit weggaan.

Verder lag er een hoop klei en groeiden er tussen de populieren mooie brandnetel- en distelperkjes.

Het volgende probleem was een vergunning. Gemeente Pijnacker wilde wel als Staatsbosbeheer het goed vond, Staatsbosbeheer wilde eerst dat een riolering geregeld was, en een riolering kon pas na toestemming van de gemeente. Deze riolering was een groot probleem; hij was er niet. Aanleggen ervan kostte ca. ƒ100.000 en - like Scouting - daar hadden we geen geld voor.

De oplossing hiervoor vonden wij in een biologische waterzuiveringsinstallatie (BZI). Een Duitse firma maakt deze dingen en -helaas voor ons- in Nederland had de overheid daar nog weinig kaas van gegeten. Wij als gebruiker moesten aantonen dat dat gezuiverde water inderdaad schoon zou zijn, maar dat kun je pas aantonen als je er een in gebruik hebt. Eind 1991 konden we deze vicieuze cirkel eindelijk doorbreken en kregen we de diverse vergunningen.

In juni 1991 begon de eerste inventarisatie van het terrein en in het najaar werd met een hoogwerker en een dieplader van het leger het oude kampwachthuisje van Raaphorst - een oude betonnen garagebox van ca. 20 ton - naar Pijnacker gebracht. Hiermee begon de bouw pas echt en werd ook het Bieslandse Bos Bulletin geboren.

Het Bulletin bleek hard nodig te zijn om mensen te informeren en vooral om ze te interesseren mee te helpen en mee te denken. De totale bouwkosten werden geschat op ƒ250.000 en we zouden door zelfwerkzaamheid dit kunnen terugbrengen tot ƒ90.000.

Door nu steeds te publiceren waar we op dat moment behoefte aan hadden, kwamen er mensen klussen, de juiste materialen brengen en - met geld - sponsoren.

Het belangrijkste effect van het bulletin was wel dat de hele achterban, Scoutingregio Den Haag, enthousiast werd voor dit weiland in Pijnacker. Hierdoor is het Bieslandse Bos nu een bloeiend natuurkampeerterrein waar Scouts kunnen genieten van een keur aan vogels, bomen en planten, en zal het ieder jaar mooier zijn dan het jaar ervoor.

In december 1991 hebben we de garagebox van Raaphorst tot toiletgebouw omgebouwd; het staat nu naast de 'schuur' van Brouwer.

Die schuur werd later omgebouwd tot kampwachthuisje. Omdat hij niet echt groot was, moesten we woekeren met de ruimte. Diverse technieken uit de scheepvaart zijn gebruikt, om tot een zo efficiënt mogelijke indeling te komen. Zo staat de wc in een hoek, en neemt daardoor nauwelijks ruimte in. Ook zijn vrijwel alle (dubbele) wanden gebruikt als bergruimte (brandblusser, lampjes, vakjes in kantoor, stoelen onder barblad, etc.) Zelfs de rugleuning van de bank is gebruikt als bergruimte.

In december hebben we ook de B.Z.I. besteld (kosten ± ƒ25.000, gesponsord door Prinses Juliana), terwijl we nog steeds niet echt zeker wisten of we voldoende geld zouden binnen krijgen om het project te laten slagen. Toch hadden we daar op dat moment zo'n groot vertrouwen in, dat we deze stap toch namen.

Ondertussen startten natuurlijk allerlei sponsoracties. Afspraak was dat één derde door bedrijven en stichtingen zou worden opgebracht, één derde door het gewest, en één derde door de scoutingleden (-groepen). 70 bedrijven werden persoonlijk aangeschreven. Dit leverde ƒ2000,- op, nog ƒ28.000,- te gaan...

Voor de groepen werd een bomensponsoractie op touw gezet. Iedereen kon voor ƒ25, een boom sponsoren, en daarmee een keer komen kamperen met zijn gezin. Twee Landelijke-Raadsleden van district Vlietstreek zijn begonnen met de actie door op de Landelijke Raad van Scouting Nederland de eerste boom te laten sponsoren door de voorzitter van de club. Tijdens en na die vergadering werden er die middag al 30 bomen 'verkocht'. Bij afsluiting van de actie (december 1992) waren in totaal 547 bomen gesponsord (meer dan ƒ13.500,-)

Door steun van stichtingen en particulieren alsook door zeer inventief regelwerk en sponsoring in natura is een voldoende groot bedrag op tafel gekomen om het terrein af te bouwen op de manier zoals het nu is.

In januari werd de waterleiding gelegd, aanvankelijk tot halverwege, maar op advies van de brandweer tot achterin het veld, omdat daar de kampvuurcirkel ligt. Vlak voordat wij de gleuf dicht wilden gooien kwam iemand met een 'paar meter elektriciteitskabel'. Dit bleek niet-goedkope grondkabel: ca. 200 meter à ƒ9,- per meter! Toen wij een elektriciteitskast kregen van het energiebedrijf (ook niet goedkoop), zat daar een kastje in met een paar schakelaars en wat elektronische zekeringen. Dit kastje had de man geruild tegen deze grondkabel.

Het resultaat is dat wij nu halverwege het terrein een aansluiting hadden op het elektriciteitsnet. Omdat daar ook een waterleiding en een draadje voor een bel of telefoon samenkomen, noemden we dat 'het Leidingveld'. Inmiddels noemen we dit het Uilenveld.

In maart waren het toiletgebouw en de B.Z.I. klaar, en kon er gekampeerd worden. Aanvankelijk eerst alleen in werk-weekeinden, waarbij de kampeerders mee moesten helpen met de bouw, vanaf 1 mei ook voor recreërende scouts. Daarna konden alle veldjes gebruikt worden en werd begonnen met de bouw van de stookbakken, de trappersbaan en het Zwaluwnest.

N.B. in elke stookplaats zitten 36 tegels en 10 kruiwagens zand verwerkt. Ook in de stookplaatsen achterin het veld.

De schuur van Brouwer werd inmiddels omgebouwd tot nieuw kampwachthuisje. Het nieuwe dak kwam er op, binnenwanden werden geplaatst en de inrichting volgde. Na de bouwvakvakantie werden een degelijk kozijn en 2 deuren geplaatst, en kon begonnen worden met de inrichting.

Op 20 juni 1992 heeft de voorzitter van Scouting Nederland, mw. Irma Gehner het terrein geopend. Zij deed dit door een kunstwerk te onthullen in aanwezigheid van allen die hadden meegeholpen, of het terrein financieel hadden mogelijk gemaakt.

Dit kunstwerk bestaat uit een plastiek van 2 door elkaar vliegende zwaluwen, en ook dit is volledig gesponsord. De ontwerpster liet zich inspireren door alle zwaluwen op en rond het terrein en in de bielzen voet zijn de drie delen van de scoutingbelofte terug te vinden.

In de zomervakantie en in het naseizoen is het terrein afgemaakt en nu kan de kampwacht in het Zwaluwnest, voorzien van alle vereiste comfort, een goed wakend oog richten op de kampeerders. De meubels, de wanden en de gehele overige inrichting zijn op maat gemaakt, om het zo efficiënt mogelijk te kunnen gebruiken. Zo zijn er 4 bedden -waarvan je er 3 kunt opklappen- die alle vier een andere maat hebben.

Aan de nesten die we in het oude dak van de stal vonden dankt 't Zwaluwnest nu zijn naam.

In het eerste jaar hebben ruim 500 kinderen genoten van een weekend of zomerkamp op dit kersverse terrein. Na 4 jaar is het terrein uitgebreid met een schuilhut, en is tegelijk het dak van de palenschuur vervangen. Na nog twee jaar is het terrein verbouwd tot hou het er nu uitziet: met vijver, met tweede pad en met opgehoogde kampeerveldjes met gras.
