---
layout: default
title: Vacatures
---

# Vacatures

Zoals iedere vrijwilligersorganisatie zijn ook wij altijd op zoek naar mensen die ons team willen aanvullen.

Voor meer informatie over onze vacatures kun je contact opnemen met [{{ site.email }}](mailto:{{ site.email }}).

## Bosonderhoud

Het terrein is eind jaren 80 aangeplant met populieren. Jaarlijks planten we honderden bomen en struiken om meer variatie te krijgen, en vooral om een bos over te houden als de populieren straks aan hun einde komen. Regelmatig moeten we bomen weghalen omdat ze slecht zijn, en daarmee gevaarlijk tijdens harde stormen.

Met name in het winterseizoen doen wij bosonderhoud, onder andere op de [Natuurwerkdag](http://www.natuurwerkdag.nl/), waarbij alle beschikbare handjes helpen.

## Technische klussers

Een technische man/vrouw die ons machinepark (de maaiers, bladblazers en motorzagen) in service heeft en gebruiksklaar houdt.

Een materiaalbeheerder (m/v) die zorgt dat alle kampeermaterialen in de ruimste zin van het woord - van de pionierpalen tot en met de bijlen en de trappersbaan - in tiptop conditie blijven.

## Kampstaf

Met name voor de periode van de zomervakanties (eind juni - begin september) zoeken wij koppels die bereid zijn om gedurende één of twee weken gastheer/gastvrouw te zijn voor onze kampeerders. Maar ook als je alleen buiten de zomerperiode beschikbaar bent, willen we graag met je praten.

Ideaal profiel: Veel Scouting ervaring, maar niet meer actief leidinggevende. Als kampwacht blijf je op een ontspannende én inspannende manier actief betrokken zonder de wekelijkse tijdsdruk.
