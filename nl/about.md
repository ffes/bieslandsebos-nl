---
layout: default
title: Over ons
---

# Over het Bieslandse Bos

Het Bieslandse Bos is een [Scouting Labelterrein](https://labelterreinen.scouting.nl/) dat [sinds 1992]({% link nl/geschiedenis.md %}) is geopend.

Het terrein wordt beheerd door een stichting waar vrijwilligers met een Scoutinghart ervoor zorgen dat het terrein, zowel technisch als organisatorisch, voor leden van Scouting een fijne plek blijft om te kamperen.

## Facebook

Volg ons op [Facebook]({{ site.social.facebook }}) <i class="bi bi-facebook text-success"></i>. Daar plaatsen wij regelmatig informatie over actuele zaken en foto's.

## Hulp gezocht

Wil je meehelpen om van het Bieslandse Bos een nog mooier en beter terrein te maken, wij hebben diverse [vacatures]({% link nl/vacatures.md %}) voor vrijwilligers.

## Archief nieuwsitems

We hebben een [archief van onze nieuwsitems]({% link nl/archief.html %}).

## Website

Zie een foutje op of ander verbeterpunt voor onze website en wil je helpen dit op te lossen? De broncode van deze site is te vinden op [GitLab](https://gitlab.com/bieslandsebos/bieslandsebos.gitlab.io) en we accepteren goede Merge Requests.
