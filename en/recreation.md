---
layout: default
title: Receation
lang: en
---

# Options at and from the campsite

<div class="col-md-4 float-end">
	<img class="img-rounded img-fluid float-end" src="{% link assets/images/tent-op-palen.jpg %}" alt="Blik op het terrein" />
</div>
The campsite lies in a wonderful nature and recreational area. There are plenty of opportunities to enjoy this.
And with various cities nearby there are many options for a [varity of activities outside the campsite]({% link en/nearby.html %}).

## Our campfire pit: Phoenix

{% assign phoenix = site.data.terrein.faciliteiten | where: "id", "phoenix" | first %}
{% for txt in phoenix.en %}
{{ txt }}
{% endfor %}

## Obstacle course

{% assign trappersbaan = site.data.terrein.faciliteiten | where: "id", "trappersbaan" | first %}
{% for txt in trappersbaan.en %}
{{ txt }}
{% endfor %}

## Recreational pond

Next to the campsite is a large swimming pond known as "De Dobbeplas". This is part of a larger recreational area, which extends from Delft, via Nootdorp and Pijnacker till the highway A12 near Zoetermeer.

The pond has many park features as well as a sandy beach, playground, sunbathing area and various paths. It is ideal for walking, raft building, swimming, rowing or fishing. Near the mill is a skeeler track and the southeast side is a paradise for birdwatchers exploring nature.

Material to [build a raft]({% link assets/downloads/vlot-en.pdf %}) is present at our campsite, and you can rent two 3-person canoes from the warden.

Tip: When everyone is happily rowing on their rafts, it is very convenient for the leaders to rent a canoe, because they can be quickly there should anything happen.


## Swimming pool

Within walking distance in Pijnacker is the indoor swimming pool [De Viergang](https://www.deviergang.nl/).
This pool is part of a [larger complex](https://www.viergang.nl/) with squash, fitness, sauna etc.
