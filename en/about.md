---
layout: default
title: About us
lang: en
---

# About Bieslandse Bos

Scout Campsite "Het Bieslandse Bos" is a campsite especially for Scouts that has opened in 1992.

The campsite is maintained by a foundation in which volenteers with a heart for Scouting make sure that the campsite is a safe and happy place to stay.

## Facebook

Follow us on [Facebook]({{ site.social.facebook }}) <i class="bi bi-facebook text-success"></i>, where we post information about recent activities and photos.

## Website

Do you spot an error on our website or see something else that can be improved? You can help.
The source code of our website is available at [GitLab](https://gitlab.com/bieslandsebos/bieslandsebos.gitlab.io) and we do accept good Merge Requests.
